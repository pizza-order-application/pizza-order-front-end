class Food {
  id!: number;
  price!: number;
  name!: string;
  orderType!: string;
  quantity: number = 0;
  imageUrl!: string;
}

export default Food;
