// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

export const APPLICATION_NAME = {
  VITO_PIZZA_SERVICE: 'http://localhost:10121',
}

export const SERVICES = {
  VITO_PIZZA: APPLICATION_NAME.VITO_PIZZA_SERVICE + '/pizza-order-service/',
}

export const environment = {
  production: false,
    ORDER: {
      GET_ALL_ORDERS:`${SERVICES.VITO_PIZZA}/order/get-all`,
    }
  }
